# Tourlane Master HTML Email Template

## Code framework

### Foundation for Emails
The Master HTML file amd snippets are based on the Foundation for Emails framework by Zurb. Full documentation for the Framwork [is available here](https://get.foundation/emails/docs/). 

There won’t be many occasions when it’s necessary to touch the code, but there are a handful of alignment and sizing classes that can be used when compiling a new layout. 

These will be highlighted when appliable in the description for each snippet.

### Accessibility headers
The header tags (especially the accessibility and dark mode features) are derived from [Mark Robbin’s Boilerplate](https://www.goodemailcode.com/email-code/template).

### Bulletproof backgrounds
Layered text and image banners c/o [Campaign Monitor](https://backgrounds.cm/). 

The MSO (Outlook) conditional formatting specifies a full width in the style `width: 1000`, but this pushes the entire layout too wide in Outlook. This has been changed to the width of either the container for full width, or the row for bordered images:

- Full width: `style="width: 680px;height:840px;`  
- Bordered width: `style="width: 590px;height:840px;`

The banner snippets already use the correct style def, this advice is only if you want to build a new block using the CM tool.

## Suggested workflow

## Directory and file structure
### Assets folder
Use this directory to store image assests for use during local development. Begin a src ref with "../assets/". All assets muct be manually copied to the CDN for live use. It’s probably best to work from the CDN from the outset, to save a find/replace step later.
### CSS folder

### Templates Folder
Conatins `master-template.php`. This is the master file. Don’t edit it, but rather duplicate and rename it.

Remember to change:

1. Language in the DOCTYPE
2. `<title>` to subject line
3. aria-label to subject line
4. lang="" to the locale of the email 

### Snippets
Contains all finalised and tested snippets.

## Snippets and blocks reference

### Header Area

`<?php include "./snipets/header-area.php" ?>`

![header area](readme_assets/headerarea.jpg)

![header area](readme_assets/headerarea_mobile.jpg)

- Include in every campaign
- Change/translate journal naviation and action bar links

---

### Hero banner with live text

`<?php include "./snipets/banner-v1.php" ?>`

![Full Width Banner](readme_assets/banner-v1.jpg)
![Full Width Banner](readme_assets/banner-v1_mobile.jpg)

- Uses Bulletproof background on outer table td
- Height can be changed in the table property (`height="840"`), but don’t change the width.
- Cover image will need to adhere to certain constraints: 
	- image 680 x 840 (display 680 x 840, retina not possible)
	- Headline graphic no lower than 550px (in source file dimensions)
- To add new banner image, replace src url in THREE places:
	- td background property: `<td class="banner" width="680" background="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/306e2c40-9911-4d82-a9ac-2c6255ab8245/1360x1680.jpg"`
	- td inline style: `style="background-image: url(http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/306e2c40-9911-4d82-a9ac-2c6255ab8245/1360x1680.jpg);`
	- mso statement: `<v:fill type="tile" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/306e2c40-9911-4d82-a9ac-2c6255ab8245/1360x1680.jpg"`

#### Known issues
- Background images not rendering in Windows 10 Mail, Android Mail 5.1, 6.0, T-Online webmail, AOL Mail (Explorer). In these instances, a dark BG will be necessary to show the text.
- Retina images not supported in GMX or Web.de. A possible workaround may be to target those clients specifically. Possible solutions here: https://howtotarget.email/

### Static Banner with border

`<?php include "./snipets/banner-static-border.php" ?>`

![Bordered Banner](readme_assets/banner-v2.jpg)

- Normal img src (not BG image)
- Replace link

### Banner + text with border

`<?php include "../snippets/banner-text-border.php" ?>`

![Bordered Banner](readme_assets/banner-v3.jpg)

- Uses Bulletproof background on inner table td
- Banner image will need to be 590 x 708 (display 590 x 708, no retina support)
- To add new banner image, replace src url in THREE places:
	- td background property: `<td class="banner" width="680" background="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/306e2c40-9911-4d82-a9ac-2c6255ab8245/1360x1680.jpg"`
	- td inline style: `style="background-image: url(http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/306e2c40-9911-4d82-a9ac-2c6255ab8245/1360x1680.jpg);`
	- mso statement: `<v:fill type="tile" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/306e2c40-9911-4d82-a9ac-2c6255ab8245/1360x1680.jpg"`

#### Known issues
See Banner hero issues, above

---

### Basic text block

`<?php include "../snippets/basic-text-block.php" ?>`

![Basic Text Block](readme_assets/basic-text-block.png)

---

### Divider

`<?php include "../snippets/divider.php" ?>`

- Desktop 90px high
- Mobile 45px high

---

### Spacers

- 15px spacer: `<?php include "../snippets/spacer-15.php" ?>`
- 30px spacer: `<?php include "../snippets/spacer-30.php" ?>`
- 45px spacer: `<?php include "../snippets/spacer-45.php" ?>`

---

### Article Rows (single & double)

- Single article row: `<?php include "../snippets/single-article-row.php" ?>`
- Double article row: `<?php include "../snippets/double-article-row.php" ?>`

![Articles](readme_assets/articles.jpg)
![Articles](readme_assets/articles_mobile.jpg)

- In order to display portrait oriented images in mobile, prepare an extra image asset in portrait orientation at 272 x 364 (displays at 137 x 182). 
	- The portrait image is hidden on desktop
- Also includes the large button snippet from `<?php include "../snippets/large-button.php" ?>`

#### Media and Asset Formating
- Large article banner 1180 px wide (any height) displays at 590 wide
- Small article banner 570 px wide (any height) displays at 285 wide

#### Known issues
- Images don’t resize to sqaure (single article) or portrait (double article) dimensions on mobile yet. Potential fix might be to include a second image sized corectly, but hidden from desktop using media query and `mso-hide:all` css property for Outlook, but this wasn’t working reliably in tests yet.

---

### Cards

- 2x2 card layout: `<?php include "../snippets/cards-2x2.php" ?>`
- Big Card with Overlay `<?php include "../snippets/card-big-overlay.php" ?>`
- Highlight card left: `<?php include "../snippets/highlight-card-left.php" ?>`
- Highlight card right: `<?php include "../snippets/highlight-card-right.php" ?>`

![Cards grid](readme_assets/cards-2x2.jpg)
![Cards grid](readme_assets/big-cards-overlay.jpg)

#### Media and Asset Formating
- images 590 (display 285) x any height, as long as all images same height

#### Known issues
- Images don’t resize to portrait dimensions on mobile yet. Potential fix might be to include a second image sized corectly, but hidden from desktop using media query and `mso-hide:all` css property for Outlook, but this wasn’t working reliably in tests yet.

---

### Big Card with overlay

