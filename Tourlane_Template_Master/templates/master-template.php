<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
  <meta name="x-apple-disable-message-reformatting">
  <meta name="color-scheme" content="light ">
  <meta name="supported-color-schemes" content="light">
  <title>Title</title>
    <style>
@import url('https://fonts.googleapis.com/css2?family=Playfair+Display:wght@700&display=swap');
</style>
<style>
<?php include "../css/foundation.css" ?>
<?php include "../css/tourlane.css" ?>
</style> 

  <!--[if mso]>
  <xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
  </xml>
  <![endif]-->

  <style>
    :root {
	color-scheme: light;
	supported-color-schemes: light;
    }
  </style>
</head>
<!-- Start Email Body. Add preheader text. Change aria-label and lang properties -->
<span class="preheader"><!-- Preheader here --></span>
<body class="body">
<div role="article" aria-roledescription="email" aria-label="email name" lang="en" style="font-size:1rem">
<table role="presentation" class="body" data-made-with-foundation>
<tr>	<td class="float-center" align="center" valign="top">
	<center>
	<table role="presentation" align="center" class="container">
	<tbody>
	<tr>	<td>
<?php include "../snippets/header-area.php" ?>
<?php include "../snippets/banner-hero.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/banner-static-border.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/banner-text-border.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/basic-text-block.php" ?>
<?php include "../snippets/divider.php" ?>
<?php include "../snippets/single-article-row.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/double-article-row.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/card-big-overlay.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/cards-2x2.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/highlight-card-left.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/highlight-card-right.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/four-small-buttons-solid.php" ?>
<?php include "../snippets/two-large-buttons-solid.php" ?>
<?php include "../snippets/spacer-45.php" ?>
<?php include "../snippets/infobox.php" ?>	
<?php include "../snippets/spacer-30.php" ?>
<?php include "../snippets/icon_text_2x2_wrapper.php" ?>	
<?php include "../snippets/divider.php" ?>
<?php include "../snippets/travel-expert.php" ?>
<?php include "../snippets/divider.php" ?>
<?php include "../snippets/funfact.php" ?>
<?php include "../snippets/divider.php" ?>
<?php include "../snippets/reviews.php" ?>
<?php include "../snippets/divider.php" ?>
<?php include "../snippets/terms.php" ?>
<?php include "../snippets/spacer-45.php" ?>		
<?php include "../snippets/footer.php" ?>

	</td>	</tr>
	</tbody>
	</table>
	</center>
</td>	</tr>
</table>
</div>
</body>
</html>
