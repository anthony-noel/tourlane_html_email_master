<!--	Start Big Card Overlay -->
	<table cellpadding="0" cellspacing="0" border="0" width="100%" >
	<tr>	<td background="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/e5dd6562-8c29-495b-ab7b-4cb8046eb7b5/1200x1440.jpg" bgcolor="#3F4144" valign="top" style="background-image: url(http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/e5dd6562-8c29-495b-ab7b-4cb8046eb7b5/1200x1440.jpg); background-position: middle right; background-repeat: no-repeat; background-size:  cover;" valign="bottom">
	      <!--[if gte mso 9]>
	      <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width: 590px;height:840px;">
	        <v:fill type="frame" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/e5dd6562-8c29-495b-ab7b-4cb8046eb7b5/1200x1440.jpg" color="#3F4144" />

	        <v:textbox inset="0,0,0,0">
	      <![endif]-->
	      <div>
		<table role="presentation" class="row">
      	      <tbody>
      	      <tr>	<th class="small-12 large-12 columns">
			<?php include "../snippets/spacer-45.php" ?>
			<table class="card">
      	      	<tr>	<th class="small-8 large-8 columns">
				<h2>A longish title breaks over three lines</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
				<table class="button small float-left radius">
				<tr>	<td>
				      <table>
				      <tr>	<td>
				      	<a href="#">Small</a>
					</td>	</tr>
				      </table>
				</td>	</tr>
				</table>
			</th> <th class="small-4 large-4 columns last">
				<table>
				<tr>	<th>
				&nbsp;
				</th>	<th class="expander">
				</th>	</tr>
				</table>
			</th></tr>
              	</table>
		</th>	<th class="expander">
		</th>	</tr>
      	      </tbody>
		</table>
		</div>
	      <!--[if gte mso 9]>
	      </v:textbox>
	      </v:rect>
	      <![endif]-->
		</td>	</tr>
		</table>
<!-- End Big Card Overlay -->
