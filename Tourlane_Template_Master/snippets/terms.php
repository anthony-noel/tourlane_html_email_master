<!-- Start Terms block -->
      	      <table role="presentation" class="row">
      	      <tbody>
      	      <tr>	<th class="small-12 large-12 columns">
	      	      <table>
	      	      <tr>	<th>
	      	      	<p class="terms">*Sie erhalten diese Email, weil Sie eine Anfrage bei <a href="https://tourlane.com">tourlane.com</a> bezüglich einer Reiseplanung erstellt haben. Sollte dies tatsächlich nicht unter Ihrem Nanem geschehen sein, kontaktieren Sie uns bitte über die oben genanten Wege und wir werden Ihre Daten umgehend aus unserem System entfernen.</p>
	      	      	</th>	<th class="expander">
		      	</th>	</tr>
		      	</table>
      	      </th></tr>
      	      </tbody>
      	      </table>
<!-- End Terms block -->
