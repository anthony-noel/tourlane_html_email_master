<!-- Start 2x2 Icon Grid -->
		<table role="presentation" class="row">
		<tbody>
		<tr>	<th class="small-6 large-6 columns first">
		<table>
		<tr>	<th>
			<table class="icon-grid">
			<tbody>
			<tr>	<th class="small-12 large-4 columns first">
				<table>
				<tr>	<th>
					<img class="float-center" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/3e97abe2-ac9f-452c-a22a-54987bac54e1/160x160.png" alt="Food and Drink" width="70" height="70" class="icon" >
					</th>	<th class="expander">
				</th>	</tr>
				</table>
			</th>	<th class="small-12 large-8 columns last">
				<table>
				<tr>	<th>
					<h4 class="kicker small-text-center">Food & Drinks</h4>
					<p class="small-text-center">Add breakfast, dinner or drinks.</p>
					</th>	<th class="expander">
				</th>	</tr>
				</table>
			</th></tr>
			</tbody>
			</table>	
			</th>	<th class="expander">
			</th>	</tr>
			</table>
		</th>	<th class="small-6 large-6 columns last">
			<table>
			<tr>	<th>
	     		<table class="icon-grid">
				<tbody>
				<tr>	<th class="small-12 large-4 columns first">
					<table>
					<tr>	<th>
						<img class="float-center" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/0e767fac-1657-49b6-b4c2-d4b1f94b1394/160x160.png" alt="Activities" width="70" height="70" class="icon" >
						</th>	<th class="expander">
					</th>	</tr>
					</table>
				</th>	<th class="small-12 large-8 columns last">
					<table>
					<tr>	<th>
						<h4 class="kicker small-text-center">Activities</h4>
						<p class="small-text-center">From wine regions to 4x4 safaris</p>
						</th>	<th class="expander">
					</th>	</tr>
					</table>
				</th></tr>
				</tbody>
				</table>
			</th>	<th class="expander">
			</th>	</tr>
			</table>
		</th>	</tr>
		<tr>	<th class="small-6 large-6 columns first">
			<table>
			<tr>	<th>
				<table class="icon-grid">
				<tbody>
				<tr>	<th class="small-12 large-4 columns first">
					<table>
					<tr>	<th>
						<img class="float-center" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/c50f74e7-13b1-4ee4-8ae0-07a1f1cac24a/160x160.png" alt="Accomodation" width="70" height="70" class="icon" >
						</th>	<th class="expander">
					</th>	</tr>
					</table>
				</th>	<th class="small-12 large-8 columns last">
					<table>
					<tr>	<th>
						<h4 class="kicker small-text-center">Accomodation</h4>
						<p class="small-text-center">A luxury hotel, campsite, or spa.</p>
						</th>	<th class="expander">
					</th>	</tr>
					</table>
				</th></tr>
				</tbody>
				</table>	
				</th>	<th class="expander">
			</th>	</tr>
			</table>
			</th>	<th class="small-6 large-6 columns last">
			<table>
			<tr>	<th>
				<table class="icon-grid">
				<tbody>
				<tr>	<th class="small-12 large-4 columns first">
					<table>
					<tr>	<th>
						<img class="float-center" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/a926972e-f177-4636-942d-46dcca221a11/160x160.png" alt="Transport" width="70" height="70" class="icon" >
						</th>	<th class="expander">
					</th>	</tr>
					</table>
				</th>	<th class="small-12 large-8 columns last">
					<table>
					<tr>	<th>
						<h4 class="kicker small-text-center">Transport</h4>
						<p class="small-text-center">Transfers to hotels and airports.</p>
						</th>	<th class="expander">
					</th>	</tr>
					</table>
				</th></tr>
				</tbody>
				</table>
				</th>	<th class="expander">
			</th>	</tr>
			</table>
		</th>	</tr>
		</tbody>
		</table>
<!-- End 2x2 Icon Grid -->
