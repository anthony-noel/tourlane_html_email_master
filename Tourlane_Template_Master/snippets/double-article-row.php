<!-- Start double article row -->
	<table role="presentation" class="row double-article">
	<tbody>
      <tr>	<th class="small-6 large-6 columns first">
	      <table>
	      <tr>	<th>
	      	<h3 class="kicker">Unterwegs Mit Tourlane</h3>
	      	<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit </h2>
	      	<img src="https://placehold.it/590x285/?text=double_article_banner" alt="Article" width="285" height="auto" class="center" >
	      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	      	</th>	<th class="expander">
      		</th>	</tr>
		<tr>	<td>
			<center>
			<table class="button small float-center  radius">
			<tr>	<td>
			      <table>
			      <tr>	<td>
			      	<a href="#">Small Button</a>
				</td>	</tr>
			      </table>
			</td>	</tr>
			</table>
			</center>
		</td>	</tr>
	      </table>
      </th>	<th class="small-6 large-6 columns last">
	      <table>
	      <tr>	<th>
	      	<h3 class="kicker">Unterwegs Mit Tourlane</h3>
	      	<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit </h2>
	      	<img src="https://placehold.it/590x285/?text=article_banner" alt="Article" width="285" height="auto" class="center" >
	      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
	      	</th>	<th class="expander">
      		</th>	</tr>
		<tr>	<td>
	      	<center>
			<table class="button small float-center  radius">
			<tr>	<td>
			      <table>
			      <tr>	<td>
			      	<a href="#">Small Button</a>
				</td>	</tr>
			      </table>
			</td>	</tr>
			</table>
			</center>
		</td>	</tr>
	      </table>
      </th> </tr>
      </tbody>
      </table>
<!-- End double article row -->
