<!-- 		Start Action Bar -->
			<table role="presentation" class="row action-bar">
			<tbody>
			<tr>	<th class="small-5 large-6 columns first">
				<table>
				<tr>	<th>
					<p><a href="#" target="_blank">Plan your trip</a></p>	
				</th>	</tr>
				</table>
			</th>	<th class="small-6 large-6 columns last">
				<table align="right">
				<tr>	<th>
					<p class="text-align-right"><a href="#" target="_blank">Call us</a></p>	
					</th>	<th class="expander">
				</th>	</tr>
				</table>
			</th></tr>
			</tbody>
			</table>
<!--  		End Action Bar -->
<!--		Start Journal navigation -->
			<table role="presentation" class="row nav show-for-large">
			<tbody>
			<tr>	<th class="small-12 large-12 columns">
				<table align="center" class="menu float-center">
				<tr>	<td>
					<table>
					<tr>	<th class="menu-item"><a href="#">Reiseziele</a></th>
					    	<th class="menu-item"><a href="#">Activitäten</a></th>
					    	<th class="menu-item last"><a href="#">Reisearten</a></th>
					</tr>
					</table>
				</td>	</tr>
				</table>
			</th>	</tr>
			</tbody>
			</table>
<!--	 	End Journal navigation -->
<!-- 		Start Logo -->
			<table background="https://via.placeholder.com/4/ffffff/ffffff&text=" role="presentation" class="row">
			<tbody>
			<tr>	<th class="small-12 large-12 columns center">
				<table>
				<tr>	<th>
					<center>
					<a href="https://www.tourlane.com/"><img class="logo float-center" width="190" height="34" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/438dd254-658d-4053-9473-5f3ea6169e88/380x68.png"></a>
					</center>
					</th>	<th class="expander">
				</th>	</tr>
				</table>
				<?php include "../snippets/spacer-45.php" ?>
			</th>	</tr>
			</tbody>
			</table>
<!--	 	End Logo -->
