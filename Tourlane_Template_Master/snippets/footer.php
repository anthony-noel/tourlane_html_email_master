<!-- Start Footer Row -->
<table class="row collapsed footer">
<tbody>  
<tr>	<th class="small-12 large-1 columns first">
      <table>
      <tr>	<th>
      	&nbsp;	
      	</th>	<th class="expander">
	</th>	</tr>
	</table>
	</th>	<th class="small-12 large-10 columns">
      <table>	
	<tr>	<th>	   
		<?php include "../snippets/spacer-45.php" ?>
		<center data-parsed="">	      
		<table align="center" class="menu float-center">
		<tr>	<td>	            
			<table>	              
			<tr>	<th class="menu-item float-center"><a href="undefined"><img alt="Facebook" width="30" height="30" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/22989860-73dc-44a0-9b03-3bcc95cf7037/60x60.png"></a>	                
			</th>	<th class="menu-item float-center"><a href="undefined"><img alt="Instagram" width="30" height="30" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/ffa087da-dda6-4751-9758-7bc83a780a94/60x60.png"></a>
			</th>	<th class="menu-item float-center"><a href="undefined"><img alt="LinkedIn" width="30" height="30" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/ec0e031e-03d0-45c3-a138-c2f216640af2/60x60.png"></a>
			</th>	<th class="menu-item float-center"><a href="undefined"><img width="30" height="30" alt="Youtube" src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/e7d1c73a-c410-4323-8116-1d4b939af215/60x60.png"></a>
			</th>	</tr>
			</table>
		</td>	</tr>
		</table>
		</center>	    
		<?php include "../snippets/spacer-15.php" ?>
		<p class="text-center">Kostenlose Rufnummer: <span class="emphasis">030 – 555708 92</span><br>	    
		email: <span class="emphasis"><a href="mailto:reiselust@tourlane.com">reiselust@tourlane.com</a></span></p>
		<?php include "../snippets/spacer-15.php" ?>
		<p class="text-center"><span class="emphasis">Tourlane GmbH</span><br />
		Köpenicker Straße 126 I 10179 Berlin</p>
		<?php include "../snippets/spacer-15.php" ?>
		<p class="text-center">&copy; 2020 Tourlane GmbH</p>	
		<p class="text-center">Sie erhalten diese Email, weil Sie eine Anfrage bei <a href="https://www.tourlane.com">tourlane.com</a> bezüglich einer Reiseplanung erstellt haben. Sollte dies tatsächlich nicht unter Ihrem Nanem geschehen sein, kontaktieren Sie uns bitte über die oben genanten Wege und wir werden Ihre Daten umgehend aus unserem System entfernen.</p>
		<p class="text-center"><a href="#">Impressum</a> | <a href="#">Datenschutz</a> | <a href="#">AGBs</a></p>
		<p class="text-center"><a href="#">Newsletter abstellen</a></p>
	</th>	<th class="expander">
	</th>	</tr>
      </table>
</th>	<th class="small-12 large-1 columns last">
      <table>
      <tr>	<th>
      	&nbsp;
      	</th>	<th class="expander">
	</th>	</tr>
	</table>
</th>	</tr>
</tbody>
</table>
<!-- End Footer Row -->
