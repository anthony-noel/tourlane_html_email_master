<!-- Start Reviews Block -->
      	      <table role="presentation" class="row">
      	      <tbody>
      	      <tr>	<th class="small-12 large-6 columns first">
	      	      <table>
	      	      <tr>	<th>
					<img src="https://placehold.it/60x60/?text=reviews" alt="User review" width="60" height="60" >
					<img src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/e658b3f0-b685-4e9d-b8c1-251b4d933f28/240x34.png" alt="Four Star rating" width="120" height="17" class="icon">
					<p><strong>Amazing experience, great trip!</strong><br />
					The Cohen Family<br />
					<span class="fade">South Africa, Cape Town</span></p>
	      	      	</th>	<th class="expander">
		      	</th>	</tr>
		      	</table>
      	      </th>	<th class="small-12 large-6 columns last">
	      	      <table>
	      	      <tr>	<th>
					<img src="https://placehold.it/60x60/?text=reviews" alt="User review" width="60" height="60" >
					<img src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/e658b3f0-b685-4e9d-b8c1-251b4d933f28/240x34.png" alt="Four Star rating" width="120" height="17" class="icon">
					<p><strong>Amazing experience, great trip!</strong><br />
					The Cohen Family<br />
					<span class="fade">South Africa, Cape Town</span></p>
	      	      	</th>	<th class="expander">
		      	</th>	</tr>
		      	</table>
      	      </th> </tr>
      	      </tbody>
      	      </table>
      	      <table role="presentation" class="row">
      	      <tbody>
      	      <tr>	<th class="small-12 large-12	columns">
      	      	<a class="trustpilot" href="https://www.trustpilot.com/review/www.tourlane.de?languages=en&stars=3&stars=4&stars=5">Read all reviews</a>	
      	      	<a href="https://www.trustpilot.com/review/www.tourlane.de?languages=en&stars=3&stars=4&stars=5"><img src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/61eb0ae1-3de9-455a-bf9d-257a984505ba/140x62.jpg" alt="TrustPilot" width="182" height="80"></a> 
				</th>	<th class="expander">
      	      </th>	</tr>
      	      </tbody>
      	      </table>
<!-- End Reviews Block -->
