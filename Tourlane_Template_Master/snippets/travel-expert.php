<!-- Start Travel Expert -->
      	      <table role="presentation" class="row">
      	      <tbody>
      	      <tr>	<th class="small-12 large-12 columns">
			<table>
			<tr>	<th>
				<table class="travel-expert reverse">
				<tr>	<th class="small-12 large-4 columns first">
	      			<img src="http://cdn.mcauto-images-production.sendgrid.net/9a4cb98a1e460340/f5992f00-b503-4adf-9613-221b2cd4933b/140x140.png" alt="Anita Werner Travel Expert" width="120" height="120" class="portrait float-center" >				      	      		
					</th>	<th class="small-12 large-8 columns last">
					<h3 class="small-text-center">Anita Werner</h3>
					<p class="small-text-center"class="small-text-center">Ihre Reiseexpertin</p>
					<p class="small-text-center">+49 30 123 456 789<br />
					<a href="mailto:a.werner@tourlane.com">a.werner@tourlane.com</a></p>
				</th>	</tr>
				</table>	      	      		
	      	</th>	<th class="expander">
			</th>	</tr>
			</table>
      	      </th>	</tr>
      	      <tr>	<th class="small-12 large-12 columns">
	      	      <table class="travel-expert te-tip">
		      	<tr>	<th>
					<h4 class="kicker">Reiseexpertin Tip</h4>
					<p>Unser Kunden lieben Namibia, Südafrika und Neuseeland. Kein Wunder, denn jedes dieser Länder ist ein Naturgigant;  alle vereinen Abenteuer, Adrenalin und Kultur. Welches Land lieben Sie?</p>
				</th>	</tr>
				</table>
	      	      </th>	<th class="expander">
		      </th>	</tr>
      	      </tbody>
      	      </table>
<!-- End Travel Expert -->
